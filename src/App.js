import './App.css';
import DialogueTree from './dialogueTree';

function App() {
  return (
    <div>
      <header>
        <DialogueTree />
      </header>
    </div>
  );
}

export default App;
